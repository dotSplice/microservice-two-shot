# Wardrobify

Team:

* Tyler - Shoes
* Brooks - hats

## Design

## Shoes microservice

The Shoes Microservice polls the wardrobe project for bin data and populates a Bin Value Object. The microservice also contains a model for Shoe data and creates an API endpoint for our frontend. The API allows us to list shoes, shoe details, and delete a shoe from the db. The Shoe Model references the BinVO with a foreign key. The bin data is used to create new shoes and store them in the appropriate closet via the API endpoint and through the UI. The microservice front-end uses the react library to render a list of our shoes via the ShoeList component. The ShoeList component fetches our Shoes and passes down the data as props to individual Shoe Components. The Shoe component has a callback to it's parent ShoeList component for the delete method and updates the state using hooks. The ShoeForm component renders a form to create a new shoe.

## Hats microservice

our hat microservice has reference to, and polls from, the wardrobe's location api via a value object.  hats are viewed in a list and can be created, deleted, or you can add new ones with access to the new hat form!
