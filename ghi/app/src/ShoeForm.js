import React, { useState, useEffect } from 'react'

function ShoeForm() {
    const [bins, setBins] = useState([])
    const [bin, setBin] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [modelName, setModelName] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')

    const fetchData = async () => {
        const BinUrl = 'http://localhost:8100/api/bins'
        const response = await fetch(BinUrl)

        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setBins(data.bins)
        }
    }

    const handleBinChange = (event) => {
        setBin(event.target.value)
    }

    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value)
    }

    const handleModelNameChange = (event) => {
        setModelName(event.target.value)
    }

    const handleColorChange = (event) => {
        setColor(event.target.value)
    }

    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.manufacturer = manufacturer
        data.model_name = modelName
        data.color = color
        data.picture_url = pictureUrl
        data.bin = bin
        console.log(data)
        const url = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)
            setBin('')
            setManufacturer('')
            setModelName('')
            setColor('')
            setPictureUrl('')

        }

    }


    useEffect(() => {
        fetchData()
    }, [])

    console.log("ShoeForm")
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Shoe</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer}/>
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleModelNameChange} placeholder="Model" required type="text" name="model" id="model" className="form-control" value={modelName}/>
                                <label htmlFor="model">Model</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={pictureUrl}/>
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleBinChange} required name="bin" id="bin" className="form-select" value={bin}>
                                <option value="">Choose a bin closet</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                                    )
                                })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Add a Shoe</button>
                        </form>
                </div>
            </div>
      </div>
    )
}

export default ShoeForm
