import React, { useState, useEffect } from 'react';
import Shoe from './Shoe';
function ShoeList() {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data.shoes)
            setShoes(data.shoes)
            }
        }

    useEffect(()=> {
        fetchData();
    }, [])

    const deleteShoe = async (id) => {

        const ShoeUrl = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(ShoeUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setShoes((prevShoes) => prevShoes.filter((shoe) => shoe.id !== id));
            }
        }


    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Image</th>
            <th>Model Name</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
                <Shoe key={shoe.id} shoe={shoe} deleteShoe={deleteShoe}/>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
