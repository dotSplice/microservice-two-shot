import React from 'react';

function Shoe(props) {

    return (
        <tr key={props.shoe.id}>
        <td>{ props.shoe.manufacturer }</td>
        <td><img style={{ width: '100px', height: '100px' }}src={ props.shoe.picture_url } /></td>
        <td>{ props.shoe.model_name }</td>
        <td>{ props.shoe.color }</td>
        <td><button className="btn btn-danger" onClick={() => props.deleteShoe(props.shoe.id)}>Delete</button></td>
      </tr>
    );}

export default Shoe
