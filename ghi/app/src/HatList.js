import React, {useState, useEffect} from 'react';

function HatList() {
  const [hats, setHats] = useState([]);

  const getData = async () => {
      const url = 'http://localhost:8090/api/hats/';
      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          setHats(data.hats);
      }
    }

  useEffect(() => {
    getData();
  }, [])

  const deleteHat = async (id) => {
    const url = `http://localhost:8090/api/hats/${id}`;
    const response = await fetch(url, {
      method: 'DELETE',
    });
    if (response.ok) {
      setHats(prevHats => prevHats.filter(hat => hat.id !== id));
    }
    }

  return (
   <table className="table table-striped table-hover">
     <thead>
       <tr>
        <th>Style</th>
        <th>Fabric</th>
        <th>Color</th>
        <th>Picture</th>
       </tr>
     </thead>

     <tbody>
       {hats.map(hat => {
         return (
          <tr className="table-secondary" key={hat.id}>
            <td>{hat.style_name}</td>
            <td>{hat.fabric}</td>
            <td>{hat.color}</td>
            <td><img className="img-thumbnail img-fluid" style={{width: '100px'}} src={hat.picture_url}  /></td>
            <td><button className="btn btn-danger" onClick={() => deleteHat(hat.id)}>Delete</button></td>
          </tr>
          );
        })}
     </tbody>
   </table>
  );
}

export default HatList;
