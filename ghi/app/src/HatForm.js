import React, {useState, useEffect} from 'react';

function HatForm() {
    const [hats, setHats] = useState([]);

    const [styleName, setStyleName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [locations, setLocations] = useState([]);
    const [location,setLocation] = useState('');


    const getData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            "style_name":styleName,
            "fabric":fabric,
            "color":color,
            "picture_url":pictureUrl,
            "location":location
        }
        console.log(data);

        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);
            setStyleName('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }

    }

    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                    <input type="text" className="form-control" id="style_name" placeholder="Style" onChange={(event) => setStyleName(event.target.value)} value={styleName} />
                    <label htmlFor="style_name">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input type="text" className="form-control" id="fabric" placeholder="Fabric" onChange={(event) => setFabric(event.target.value)} value={fabric} />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input type="text" className="form-control" id="color" placeholder="Color" onChange={(event) => setColor(event.target.value)} value={color} />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input type="text" className="form-control" id="picture_url" placeholder="Picture" onChange={(event) => setPictureUrl(event.target.value)} value={pictureUrl} />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="form-floating mb-3">
                    <select className="form-select" id="location" onChange={(event) => setLocation(event.target.value)} value={location} >
                        <option value="">Select Location</option>
                        {locations.map(location => {
                            return <option key={location.id} value={location.href}>{location.closet_name}</option>
                        })}
                    </select>
                <button className="w-100 btn btn-lg btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    )
}

export default HatForm;
