import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeList from './ShoeList';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

import ShoeForm from './ShoeForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList hats={props.hats} />} />
          <Route path="/new-hat" element={<HatForm />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="/new-shoe" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
