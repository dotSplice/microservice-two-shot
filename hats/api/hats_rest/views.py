from common.json import ModelEncoder
from .models import Hat, LocationVO
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'closet_name',
        'section_number',
        'shelf_number',
        'import_href']


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric', 'style_name', 'color', 'picture_url', 'id']
    encoders = {
        "location": LocationVOEncoder()
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric', 'style_name', 'color', 'picture_url', 'id']
    encoders = {
        "location": LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        data = json.loads(request.body)
        print(data)
        try:
            location_href = data["location"]
            location = LocationVO.objects.get(import_href=location_href)
            data["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location not found"},
                status=400,
            )

        new_hat = Hat.objects.create(**data)
        return JsonResponse(
            new_hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def hat_detail(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat not found"},
                status=404,
            )
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse(
                {"message": "Hat deleted"},
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Hat not found"},
                status=404,
            )
