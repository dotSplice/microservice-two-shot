from django.db import models


class LocationVO(models.Model):
    import_href = models.URLField(max_length=300, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(max_length=300)

    location = models.ForeignKey(
        LocationVO, related_name='hats', on_delete=models.CASCADE
    )

    def __str__(self):
        return self.style_name
