from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json

from common.json import ModelEncoder

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "model_name", "color", "picture_url"]

    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "manufacturer", "model_name", "color", "picture_url"]
    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "DELETE"])
def api_show_shoe_detail(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            shoe, encoder=ShoeDetailEncoder, safe=False
        )

    if request.method == "DELETE":
        Shoe.objects.get(pk=pk).delete()
        return JsonResponse({"message": "deleted"})


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
